package me.charliejc.luck.util;

import me.charliejc.luck.wrappers.Mappings;

import java.lang.instrument.Instrumentation;

public class ClassDiscoverer {
    private Class<?> minecraftClass;
    private Class<?> entityClass;
    private Class<?> entityLivingBaseClass;
    private Class<?> gameSettings;
    private Class<?> sessionClass;
    private Class<?> packetDispatcher;
    private Class<?> clientTickHandler;

    public ClassDiscoverer(Instrumentation instrumentation) {
        for (Class<?> clazz : instrumentation.getAllLoadedClasses()) {
            switch (clazz.getName()) {
                case Mappings.CLASS_MINECRAFT:
                    this.minecraftClass = clazz;
                    break;
                case Mappings.CLASS_ENTITY:
                    this.entityClass = clazz;
                    break;
                case Mappings.CLASS_ENTITYLIVINGBASE:
                    this.entityLivingBaseClass = clazz;
                    break;
                case Mappings.CLASS_GAMESETTINGS:
                    this.gameSettings = clazz;
                    break;
                case Mappings.CLASS_SESSION:
                    this.sessionClass = clazz;
                    break;
                case Mappings.CLASS_PACKETDISPATCHER:
                    this.packetDispatcher = clazz;
                    break;
                case Mappings.CLASS_CLIENT_TICK_HANDLER:
                    this.clientTickHandler = clazz;
                    break;
            }
        }
    }

    public Class<?> getMinecraftClass() {
        return minecraftClass;
    }

    public Class<?> getEntityClass() {
        return entityClass;
    }

    public Class<?> getEntityLivingBaseClass() {
        return entityLivingBaseClass;
    }

    public Class<?> getGameSettingsClass() {
        return gameSettings;
    }


    public Class<?> getSessionClass() {
        return sessionClass;
    }

    public Class<?> getPacketDispatcher() {
        return packetDispatcher;
    }

    public Class<?> getClientTickHandler() {
        return clientTickHandler;
    }
}
