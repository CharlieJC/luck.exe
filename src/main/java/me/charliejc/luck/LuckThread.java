package me.charliejc.luck;

import me.charliejc.luck.mods.Mod;
import me.charliejc.luck.wrappers.Minecraft;

public class LuckThread extends Thread {
    private Luck parent;

    public LuckThread(Luck parent) {
        this.parent = parent;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Minecraft.clientPlayer.playerObject = Minecraft.clientPlayer.playerField.get(Minecraft.getMinecraft().getMinecraftObject());
                for (Mod mod : this.parent.getMods()) {
                    if (mod.isEnabled()) mod.update();
                }
                Thread.sleep(50L);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
