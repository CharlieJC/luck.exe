package me.charliejc.luck.mods;

import me.charliejc.luck.wrappers.ClientTickHandler;

import java.lang.reflect.Field;

public class NoSpread extends Mod {

    Field spreadSprinting;
    Field spreadJumping;
    Field spreadWalking;
    Field spreadWalkingMax;

    public NoSpread() {
        super("NoSpread");
        try {
            spreadSprinting = ClientTickHandler.getClientTickHandler().getClientTickHandlerClass().getDeclaredField("spreadSprinting");
            spreadJumping = ClientTickHandler.getClientTickHandler().getClientTickHandlerClass().getDeclaredField("spreadJumping");
            spreadWalking = ClientTickHandler.getClientTickHandler().getClientTickHandlerClass().getDeclaredField("spreadWalking");
            spreadWalkingMax = ClientTickHandler.getClientTickHandler().getClientTickHandlerClass().getDeclaredField("spreadWalkingMax");
        } catch (NoSuchFieldException ex) {
        }
    }

    @Override
    public void update() throws Exception {
        spreadSprinting.setFloat(ClientTickHandler.getClientTickHandler().getClientTickHandlerClass(), 0);
        spreadJumping.setFloat(ClientTickHandler.getClientTickHandler().getClientTickHandlerClass(), 0);
        spreadWalking.setFloat(ClientTickHandler.getClientTickHandler().getClientTickHandlerClass(), 0);
        spreadSprinting.setFloat(ClientTickHandler.getClientTickHandler().getClientTickHandlerClass(), 0);
    }
}
