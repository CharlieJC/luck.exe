package me.charliejc.luck.mods;

import me.charliejc.luck.wrappers.Minecraft;

public class FullBright extends Mod {
    public FullBright() {
        super("Full Bright");
    }

    @Override
    public void update() throws Exception {
    }

    @Override
    public void onDisable() throws Exception {
        Minecraft.getMinecraft().getGameSettings().setGammaSetting(10.0F);
    }
}
