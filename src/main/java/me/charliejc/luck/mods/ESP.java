package me.charliejc.luck.mods;

import me.charliejc.luck.wrappers.Mappings;
import me.charliejc.luck.wrappers.PlayerEntity;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.stream.Collectors;

import static me.charliejc.luck.mods.Mod.minecraftObject;

public class ESP extends Mod{
    private Field theWorld;
    private Field playerEntities;
    private Field inGameHasFocus;

    public ESP() {
        super("ESP");
        try {
            this.theWorld = minecraftClass.getField(Mappings.theWorld);
            this.playerEntities = this.theWorld.getType().getField(Mappings.playerEntities);
            this.inGameHasFocus = minecraftClass.getField(Mappings.inGameHasFocus);
        } catch (NoSuchFieldException ex) {
            ex.printStackTrace();
        }
    }

//    @Override
//    public void update() throws Exception {
//        boolean inGameHasFocus = this.inGameHasFocus.getBoolean(minecraftObject);
//        if (!inGameHasFocus) return;
//
//        List<PlayerEntity> nearbyPlayers = (List<PlayerEntity>) this.playerEntities.get(this.theWorld.get(minecraftObject));
//        List<PlayerEntity> targets = nearbyPlayers.stream()
//                .filter(target -> target != null)
//                .filter(target -> target.isEntityAlive())
//                .filter(target -> {
//                    try {
//                        return !target.isInvisible();
//                    } catch (IllegalAccessException | InvocationTargetException ex) {
//                        ex.printStackTrace();
//                    }
//                    return false;
//                })
//                .collect(Collectors.toList());


//        if (target == null) return;
//        if (!target.isEntityAlive()) return;
//        if (target.isInvisible()) return;
//    }

}
