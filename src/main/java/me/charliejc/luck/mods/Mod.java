package me.charliejc.luck.mods;

import me.charliejc.luck.wrappers.Minecraft;

public class Mod {
    private String name;
    private boolean enabled = false;
    protected static Object minecraftObject;
    protected static Class minecraftClass;

    public Mod(String name) {
        this.name = name;
        minecraftClass = Minecraft.getMinecraft().getMinecraftClass();
        minecraftObject = Minecraft.getMinecraft().getMinecraftObject();
    }

    public void update() throws Exception{}

    public void onEnable() throws Exception{}

    public void onDisable() throws Exception{}

    public String getName() {
        return name;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isEnabled() {
        return enabled;
    }
}
