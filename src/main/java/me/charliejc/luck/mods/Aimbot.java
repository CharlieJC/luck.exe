package me.charliejc.luck.mods;

import me.charliejc.luck.Luck;
import me.charliejc.luck.util.Chat;
import me.charliejc.luck.wrappers.ClientPlayerr;
import me.charliejc.luck.wrappers.Mappings;
import me.charliejc.luck.wrappers.Minecraft;
import me.charliejc.luck.wrappers.PlayerEntity;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

public class Aimbot extends Mod {
    private Field theWorld;
    private Field playerEntities;
    private Field inGameHasFocus;


    private Class teamsClass;

//MAKE IT COMPATIBLE WITH FEET
    public Aimbot() {
        super("Aimbot");
        try {
            this.theWorld = minecraftClass.getField(Mappings.theWorld);
            this.playerEntities = this.theWorld.getType().getField(Mappings.playerEntities);
            this.inGameHasFocus = minecraftClass.getField(Mappings.inGameHasFocus);
        } catch (NoSuchFieldException ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void update() throws Exception {
        System.out.println("Debug:");
        boolean inGameHasFocus = this.inGameHasFocus.getBoolean(minecraftObject);
        if (!inGameHasFocus) {
//            System.out.println("inGameHasFocus = false");
            return;
        }
        PlayerEntity target = getNearestPlayer();

        if (target == null) {
//            System.out.println("target = null");
            return;
        }
        if (!Minecraft.clientPlayer.canEntityBeSeen(target.getEntityObject())) {
//            System.out.println("canEntityBeSeen = false");
            return;
        }
        if (!target.isEntityAlive()) {
//            System.out.println("isEntityAlive = false");
            return;
        }
        if (target.isInvisible()) {
//            System.out.println("isInvisible = true");
            return;
        }

        float[] entityRotations = getEntityRotations(Minecraft.clientPlayer, target);
        Minecraft.clientPlayer.setViewAngles(entityRotations[0], entityRotations[1]);
    }


    private PlayerEntity getNearestPlayer() throws IllegalAccessException, InvocationTargetException, NoSuchFieldException {
        PlayerEntity playerEntity = null;
        if (this.playerEntities == null) {
            System.out.println("PLAYERENTITIES IS NULL");
            return null;
        }
        List<Object> nearbyPlayers = (List) this.playerEntities.get(this.theWorld.get(minecraftObject));
        StringBuilder builder = new StringBuilder("");
        if (nearbyPlayers == null || nearbyPlayers.isEmpty()) return null;
        for (Object playerObject : nearbyPlayers) {
            PlayerEntity temp = new PlayerEntity(playerObject);
            builder.append(temp.getUsername()).append(",");
            if (Minecraft.clientPlayer.canEntityBeSeen(playerObject) && !temp.getUsername().equals(Minecraft.clientPlayer.getUsername()) && !temp.isInvisible() && !Luck.getInstance().getUI().getFriends().contains(temp.getUsername()) && temp.isEntityAlive()) {

                if (playerEntity == null) {
                    playerEntity = temp;
                }
                if (getDistanceToEntity(Minecraft.clientPlayer, temp) < getDistanceToEntity(Minecraft.clientPlayer, playerEntity)) {
                    playerEntity = temp;
                }
            }
        }
//        System.out.println("NearbyPlayers = " + builder.toString());
        return playerEntity;
    }


    private float getDistanceToEntity(ClientPlayerr clientPlayer, PlayerEntity playerEntity) throws IllegalAccessException {
        float x = (float) (playerEntity.getPosX() - clientPlayer.getPosX());
        float y = (float) (playerEntity.getPosY() - clientPlayer.getPosY());
        float z = (float) (playerEntity.getPosZ() - clientPlayer.getPosZ());
        return (float) Math.sqrt(x * x + y * y + z * z);
    }

    public static float[] getEntityRotations(ClientPlayerr clientPlayer, PlayerEntity target) throws IllegalAccessException {
        double posX = target.getPosX() - clientPlayer.getPosX();
        double posY = target.getPosY() - clientPlayer.getPosY() + 1.5D;
        double posZ = target.getPosZ() - clientPlayer.getPosZ();
        double distanceXZ = (float) Math.sqrt(posX * posX + posZ * posZ);
        float yaw = (float) (Math.atan2(posZ, posX) * 180.0D / Math.PI) - 90.0F;
        float pitch = (float) -(Math.atan2(posY, distanceXZ) * 180.0D / Math.PI);
        return new float[]{yaw, pitch};
    }
}
