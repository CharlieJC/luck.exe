package me.charliejc.luck.ui;

import me.charliejc.luck.Luck;
import me.charliejc.luck.mods.Mod;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.util.Arrays;

public class LuckUI {

    private Luck parent;
    private JFrame frame;
    private JTextField friends;

    private static final Color BACKGROUND = new Color(30, 53, 53);
    private static final Color FOREGROUND = new Color(254, 255, 140);
    private static final BevelBorder INPUT_BORDER = (BevelBorder) BorderFactory.createLoweredBevelBorder();
    private static final BevelBorder BUTTON_BORDER = (BevelBorder) BorderFactory.createRaisedBevelBorder();


    public LuckUI(Luck parent) {
        this.parent = parent;
    }

    public void show() {
//
//        frame = new JFrame("Luck.exe");
//        frame.addWindowListener(new LuckWindowListener(this.parent));
//        frame.setVisible(true);
//        frame.setLayout(null);
//        frame.setSize(new Dimension(400, 300));
//        frame.setBackground(BACKGROUND);
//
//        //mods
//        JPanel main = new JPanel();
//        main.setBackground(BACKGROUND);
//        for (Mod mod : this.parent.getMods()) {
//            JCheckBox checkBox = new JCheckBox(mod.getName());
//            checkBox.setBackground(BACKGROUND);
//            checkBox.setForeground(FOREGROUND);
//            checkBox.setBorder(BUTTON_BORDER);
//            checkBox.addItemListener(new ModListener(mod));
//            main.add(checkBox);
//        }
//
//
//        //friends
//        friends = new JTextField();
//        friends.setText("name1,name2,name3");
//        main.setBackground(BACKGROUND);
//        main.setForeground(FOREGROUND);
//        main.setBorder(INPUT_BORDER);
//        JTextField friendsTitle = new JTextField();
//        main.setBackground(BACKGROUND);
//        main.setForeground(FOREGROUND);
//        main.setBorder(BUTTON_BORDER);
//        main.add(friendsTitle);
//        main.add(friends);
//        frame.add(main);
//
//
//        //pack window
//        frame.pack();

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            JFrame frame = new JFrame();
            frame.setResizable(false);
            frame.setTitle("Luck.exe - By AllSkill");
            frame.setSize(406, 330);
            frame.setLocationRelativeTo(null);
            frame.setDefaultCloseOperation(3);
            frame.setLayout(null);
            frame.setBackground(BACKGROUND);

            JTextField infofield = new JTextField("AllSkill is god");
            infofield.setEditable(false);
            infofield.setBounds(0, 0, 400, 20);
            infofield.setBackground(BACKGROUND);
            infofield.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 12));
            infofield.setForeground(FOREGROUND);
            infofield.setBorder(BUTTON_BORDER);
            frame.add(infofield);


            int index = 0;
            for (Mod mod : this.parent.getMods()) {
                JCheckBox checkBox = new JCheckBox(mod.getName());
                checkBox.setBackground(BACKGROUND);
                checkBox.setForeground(FOREGROUND);
                checkBox.setBorder(INPUT_BORDER);
                checkBox.addItemListener(new ModListener(mod));
                checkBox.setBounds((index * 100), 20 + (20 * checkboxHeight(index)), 100, 20);
                frame.add(checkBox);
                index++;
            }

            int friendsTitleHeight = (20 * checkboxHeight(index)) + 40;

            JTextField friendTitle = new JTextField("Friends:");
            friendTitle.setEditable(false);
            friendTitle.setBounds(0, friendsTitleHeight, 400, 20);
            friendTitle.setBackground(BACKGROUND);
            friendTitle.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 12));
            friendTitle.setForeground(FOREGROUND);
            friendTitle.setBorder(BUTTON_BORDER);
            frame.add(friendTitle);


            friends = new JTextField();
            friends.setBounds(0, friendsTitleHeight + 20, 400, 20);
            friends.setText("Name1:Name2:Name3");
            friends.setBackground(BACKGROUND);
            friends.setForeground(FOREGROUND);
            friends.setBorder(INPUT_BORDER);
            frame.add(friends);


            frame.setVisible(true);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void destroy() {
        frame.dispose();
    }

    public java.util.List<String> getFriends() {
        String[] friendsArray = friends.getText().split(":");
        return Arrays.asList(friendsArray);
    }

    private int checkboxHeight(int index) {
        int modulus = index % 4;
        int height = index / 4 - modulus;

        if (height < 0) return 0;
        return height;
    }
}
