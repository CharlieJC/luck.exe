package me.charliejc.luck.ui;

import me.charliejc.luck.Luck;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class LuckWindowListener implements WindowListener {
    private Luck parent;

    public LuckWindowListener(Luck parent) {
        this.parent = parent;
    }

    @Override
    public void windowClosing(WindowEvent e) {
        this.parent.closing();
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
