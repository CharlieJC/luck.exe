package me.charliejc.luck.ui;

import me.charliejc.luck.mods.Mod;
import me.charliejc.luck.util.Chat;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class ModListener implements ItemListener {
    private Mod mod;

    public ModListener(Mod mod) {
        this.mod = mod;
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        boolean bool = e.getStateChange() == 1;
        this.mod.setEnabled(bool);
        try {
            if (bool) {
                this.mod.onEnable();
//                Chat.sendMessage(Chat.GOLD + Chat.BOLD + this.mod.getName() + " enabled!");
            } else {
                this.mod.onDisable();
//                Chat.sendMessage(Chat.GOLD + Chat.BOLD + this.mod.getName() + " disabled!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
