package me.charliejc.luck.agent;

import me.charliejc.luck.Luck;

import java.io.IOException;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.lang.instrument.Instrumentation;
import java.lang.instrument.UnmodifiableClassException;
import java.security.ProtectionDomain;

public class Agent implements ClassFileTransformer {

    private static Agent agent = null;
    private Instrumentation instrumentation;

    private static byte[] PATCHED_BYTES;
    private static final String PATH_TO_FILE = "bcy.class";
    private static final String CLASS_TO_PATCH = "net.minecraft.client.multiplayer.GuiConnecting";

    public static Agent getAgent() {
        return agent;
    }

    public Agent(Instrumentation inst) {
        this.instrumentation = inst;

        try {
            for (Class<?> clazz : this.instrumentation.getAllLoadedClasses()) {
                if (clazz.getName().equals("net.minecraft.client.multiplayer.GuiConnecting")) {
                    System.out.println("*************************************************************");
                    System.out.println("*************************************************************");
                    System.out.printf("RETRANSFORMING");
                    System.out.println("*************************************************************");
                    System.out.println("*************************************************************");
                    this.instrumentation.retransformClasses(clazz);

                }
            }
        } catch (UnmodifiableClassException ex) {
            ex.printStackTrace();
        }

    }

    public static void agentmain(String s, Instrumentation inst) {
        agent = new Agent(inst);
        System.out.printf("Got here");
        inst.addTransformer(agent);
        System.out.printf("Got here1");
        new Luck(inst);
        System.out.printf("Got here2");

    }

    @Override
    public byte[] transform(final ClassLoader loader, String className, final Class classBeingRedefined, final ProtectionDomain protectionDomain, final byte[] classfileBuffer) throws IllegalClassFormatException {

        try {
//            for (Class clazz : instrumentation.getAllLoadedClasses()){
//                if (clazz.getName().equals(CLASS_TO_PATCH)) {
//                    System.out.println(CLASS_TO_PATCH + " is loaded");
//                    if (classBeingRedefined.equals(clazz)) {
//                        System.out.println("Patching... " + className);
//                        return Tools.getBytesFromResource(loader, PATH_TO_FILE);
//                    } else {
//                        System.out.println("Class " + CLASS_TO_PATCH + " not found...");
//                    }
//                }
//            }
//            if (className != null) {
//                System.out.println(className);
//            }
            if (className != null && className.equals("net/minecraft/client/multiplayer/GuiConnecting")) {
                System.out.println("Patching... " + className);
                return Tools.getBytesFromResource(loader, PATH_TO_FILE);
            }
            if (className != null && className.equals("bcy")) {
                System.out.println("Patching... " + className);
                return Tools.getBytesFromResource(loader, PATH_TO_FILE);
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return classfileBuffer;
    }

//    private byte[] readPatchedCode(String pathString) throws IOException{
//        URL url = ClassLoader.getSystemResource(pathString);
//        Path path = Paths.get(pathString);
//        return Files.readAllBytes(path);
//    }

//    public byte[] patchClassInJar(String name, byte[] bytes, String ObfName, File location)
//    {
//        try
//        {
//            ZipFile zip = new ZipFile(location);
//            ZipEntry entry = zip.getEntry(name.replace('.', '/') + ".class");
//            if (entry == null)
//            {
//                System.out.println(name + " not found in " + location.getName());
//            }
//            else
//            {
//                InputStream zin = zip.getInputStream(entry);
//                bytes = new byte[(int)entry.getSize()];
//                zin.read(bytes);
//                zin.close();
//                System.out.println("[Haxxes]: Class " + name + " patched!");
//            }
//            zip.close();
//        }
//        catch (Exception e)
//        {
//            throw new RuntimeException("Error overriding " + name + " from " + location.getName(), e);
//        }
//        return bytes;
//    }
}
