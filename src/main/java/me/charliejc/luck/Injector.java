package me.charliejc.luck;

import com.sun.tools.attach.*;
import me.charliejc.luck.agent.Agent;
import me.charliejc.luck.agent.AgentLoader;
import me.charliejc.luck.agent.Tools;
import me.charliejc.luck.mods.*;
import me.charliejc.luck.ui.LuckUI;
import me.charliejc.luck.ui.LuckWindowListener;
import me.charliejc.luck.ui.ModListener;
import me.charliejc.luck.util.Chat;
import me.charliejc.luck.util.ClassDiscoverer;
import me.charliejc.luck.wrappers.*;

import java.io.IOException;

public class Injector {
    public void inject() {
        Tools.loadAgentLibrary();

        //Look for minecraft jvm
        System.out.println("Scanning for Minecraft JVM...");
        String pid = null;
        for (VirtualMachineDescriptor vm : VirtualMachine.list()) {
            if (vm.displayName().startsWith("net.minecraft.launchwrapper.Launch")) {
                pid = vm.id();
                //Found jvm
                System.out.println("Found Minecraft JVM!");
            }
        }

        if (pid == null) {
            //Didn't find jvm
            System.out.println("Couldn't find JVM");
            return;
        }

        try {
            AgentLoader.attachAgentToJVM(pid, Agent.class, LuckUI.class, LuckWindowListener.class, Luck.class, LuckThread.class, ClassDiscoverer.class, Mod.class, Mappings.class, Minecraft.class, ClientPlayerr.class, ModListener.class, Aimbot.class, PlayerEntity.class, Chat.class, GameSettings.class, FullBright.class, NoSpread.class,ESP.class,PacketDispatcher.class,ClientTickHandler.class);
        } catch (IOException | AttachNotSupportedException | AgentLoadException | AgentInitializationException ex) {
            ex.printStackTrace();
        }
    }

}
