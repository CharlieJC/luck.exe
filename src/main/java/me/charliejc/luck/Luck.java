package me.charliejc.luck;

import me.charliejc.luck.mods.*;
import me.charliejc.luck.ui.LuckUI;
import me.charliejc.luck.util.ClassDiscoverer;
import me.charliejc.luck.wrappers.ClientTickHandler;
import me.charliejc.luck.wrappers.Minecraft;

import java.lang.instrument.Instrumentation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Luck {
    private ClassDiscoverer discoverer;
    private Instrumentation instrumentation;
    private List<Mod> mods = Collections.synchronizedList(new ArrayList<>());
    private LuckThread luckThread;
    private static Luck instance;
    private LuckUI ui;

    public Luck(Instrumentation instrumentation) {
        this.instrumentation = instrumentation;
        this.discoverer = new ClassDiscoverer(instrumentation);
        instance = this;
        new Minecraft();
        new ClientTickHandler();

        try {
            this.mods.add(new Aimbot());
            this.mods.add(new FullBright());
            this.mods.add(new NoSpread());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        ui = new LuckUI(this);
        ui.show();

        this.luckThread = new LuckThread(this);
        this.luckThread.start();

    }

    public void closing() {
        this.luckThread.stop();
        this.mods.clear();

    }

    public Instrumentation getInstrumentation() {
        return instrumentation;
    }

    public List<Mod> getMods() {
        return mods;
    }

    public static void main(String[] args) throws Exception {
        new Injector().inject();
    }

    public ClassDiscoverer getDiscoverer() {
        return discoverer;
    }

    public static Luck getInstance() {
        return instance;
    }

    public LuckUI getUI() {
        return ui;
    }
}
