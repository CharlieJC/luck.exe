package me.charliejc.luck.wrappers;

import me.charliejc.luck.Luck;

import java.lang.instrument.Instrumentation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ClientPlayerr {
    public Object playerObject;
    public Field playerField;
    private Class<?> playerClass;
    private Class<?> axisAlignedBBClass;
    private Field rotationYaw;
    private Field rotationPitch;
    private Field posX;
    private Field posY;
    private Field posZ;
    private Field username;
    private Method canEntityBeSeen;
    private Method addChatMessage;

    public ClientPlayerr(Minecraft minecraft) {
        try {
            this.playerField = minecraft.getMinecraftClass().getField(Mappings.thePlayer);
            this.playerClass = this.playerField.getType();

            this.rotationYaw = this.playerClass.getField(Mappings.rotationYaw);
            this.rotationPitch = this.playerClass.getField(Mappings.rotationPitch);

            this.posX = this.playerClass.getField(Mappings.posX);
            this.posY = this.playerClass.getField(Mappings.posY);
            this.posZ = this.playerClass.getField(Mappings.posZ);

            this.canEntityBeSeen = this.playerClass.getMethod(Mappings.canEntityBeSeen, Luck.getInstance().getDiscoverer().getEntityClass());
            this.addChatMessage = this.playerClass.getMethod(Mappings.addChatMessage, String.class);

            this.playerObject = this.playerField.get(minecraft.getMinecraftObject());

            this.username = this.playerClass.getField(Mappings.username);
        } catch (NoSuchFieldException | NoSuchMethodException | IllegalAccessException ex) {
            ex.printStackTrace();
        }
    }

    public String getUsername() throws IllegalAccessException {
        return (String) this.username.get(this.playerObject);
    }

    public void sendMessage(String message) throws IllegalAccessException, InvocationTargetException {
        this.addChatMessage.invoke(playerObject, message);
    }

    public boolean canEntityBeSeen(Object entityObject) throws IllegalAccessException, InvocationTargetException {
        return (boolean) this.canEntityBeSeen.invoke(playerObject, entityObject);
    }

    public float[] getViewAngles()
            throws IllegalAccessException {
        return new float[]{getYaw(), getPitch()};
    }

    public void setViewAngles(float yaw, float pitch)
            throws IllegalAccessException {
        setYaw(yaw);
        setPitch(pitch);
    }


    public float getYaw()
            throws IllegalAccessException {
        return this.rotationYaw.getFloat(this.playerObject);
    }

    public float getPitch()
            throws IllegalAccessException {
        return this.rotationPitch.getFloat(this.playerObject);
    }

    public void setYaw(float yaw)
            throws IllegalAccessException {
        this.rotationYaw.setFloat(this.playerObject, yaw);
    }

    public void setPitch(float pitch)
            throws IllegalAccessException {
        this.rotationPitch.setFloat(this.playerObject, pitch);
    }

    public double getPosX()
            throws IllegalAccessException {
        return this.posX.getDouble(this.playerObject);
    }

    public double getPosY()
            throws IllegalAccessException {
        return this.posY.getDouble(this.playerObject);
    }

    public double getPosZ()
            throws IllegalAccessException {
        return this.posZ.getDouble(this.playerObject);
    }
}