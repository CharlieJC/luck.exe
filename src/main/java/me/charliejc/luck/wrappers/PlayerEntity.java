package me.charliejc.luck.wrappers;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class PlayerEntity {

    private Object entityObject;
    private Class<?> entityClass;
    private Field rotationYaw;
    private Field rotationPitch;
    private Field posX;
    private Field posY;
    private Field posZ;
    private Field username;
    private Method isEntityAlive;
    private Method isInvisible;

    public PlayerEntity(Object entityObject) {
        try {
            this.entityObject = entityObject;
            this.entityClass = entityObject.getClass();

            //Entity angles
            this.rotationYaw = this.entityClass.getField(Mappings.rotationYaw);
            this.rotationPitch = this.entityClass.getField(Mappings.rotationPitch);

            //Entity XYZ coordinates
            this.posX = this.entityClass.getField(Mappings.posX);
            this.posY = this.entityClass.getField(Mappings.posY);
            this.posZ = this.entityClass.getField(Mappings.posZ);

            //Entity username
            this.username = this.entityClass.getField(Mappings.username);

            //Entity methods
            this.isInvisible = this.entityClass.getMethod(Mappings.isInvisible);
            this.isEntityAlive = this.entityClass.getMethod(Mappings.isEntityAlive);
        } catch (NoSuchFieldException | NoSuchMethodException ex) {
            ex.printStackTrace();
        }
    }

    public boolean isInvisible() throws IllegalAccessException, InvocationTargetException {
        return (boolean) this.isInvisible.invoke(entityObject);
    }

    public boolean isEntityAlive() throws IllegalAccessException, InvocationTargetException {
        return (boolean) this.isEntityAlive.invoke(entityObject);
    }

    public String getUsername() throws IllegalAccessException {
        return (String) this.username.get(this.entityObject);
    }

    public Object getEntityObject() {
        return entityObject;
    }

    public float[] getViewAngles()
            throws IllegalAccessException {
        return new float[]{getYaw(), getPitch()};
    }

    public void setViewAngles(float yaw, float pitch)
            throws IllegalAccessException {
        setYaw(yaw);
        setPitch(pitch);
    }

    public float getYaw()
            throws IllegalAccessException {
        return this.rotationYaw.getFloat(this.entityObject);
    }

    public float getPitch()
            throws IllegalAccessException {
        return this.rotationPitch.getFloat(this.entityObject);
    }

    public void setYaw(float yaw)
            throws IllegalAccessException {
        this.rotationYaw.setFloat(this.entityObject, yaw);
    }

    public void setPitch(float pitch)
            throws IllegalAccessException {
        this.rotationPitch.setFloat(this.entityObject, pitch);
    }

    public double getPosX()
            throws IllegalAccessException {
        return this.posX.getDouble(this.entityObject);
    }

    public double getPosY()
            throws IllegalAccessException {
        return this.posY.getDouble(this.entityObject);
    }

    public double getPosZ()
            throws IllegalAccessException {
        return this.posZ.getDouble(this.entityObject);
    }
}
