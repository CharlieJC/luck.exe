package me.charliejc.luck.wrappers;

import me.charliejc.luck.Luck;

import java.lang.reflect.Field;

public class GameSettings {

    private Object gameSettingsObject;
    private Class<?> gameSettingsClass;
    private Field gammaSetting;

    public GameSettings(Object gameSettingsObject) {
        this.gameSettingsClass = Luck.getInstance().getDiscoverer().getGameSettingsClass();
        this.gameSettingsObject = gameSettingsObject;

        try {
            this.gammaSetting = this.gameSettingsClass.getField(Mappings.gammaSetting);
        } catch (NoSuchFieldException ex) {
            ex.printStackTrace();
        }
    }

    public void setGammaSetting(float gamma) throws IllegalAccessException {
        this.gammaSetting.setFloat(this.gameSettingsObject, gamma);
    }
}
