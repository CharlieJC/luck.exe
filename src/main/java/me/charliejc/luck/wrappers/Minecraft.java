package me.charliejc.luck.wrappers;

import me.charliejc.luck.Luck;

import java.lang.reflect.InvocationTargetException;

public class Minecraft {
    private static Minecraft minecraft;
    public static ClientPlayerr clientPlayer;
    private Object minecraftObject;
    private Class<?> minecraftClass;
    private GameSettings gameSettings;

    public Minecraft() {
        this.minecraftClass = Luck.getInstance().getDiscoverer().getMinecraftClass();

        try {
            this.minecraftObject = minecraftClass.getMethod(Mappings.getMinecraft).invoke(null);
            this.gameSettings = new GameSettings(this.minecraftClass.getField(Mappings.gameSettings).get(this.minecraftObject));

        } catch (NoSuchMethodException | NoSuchFieldException | IllegalAccessException | InvocationTargetException ex) {
            ex.printStackTrace();
        }

        minecraft = this;
        clientPlayer = new ClientPlayerr(this);
    }

    public Object getMinecraftObject() {
        return minecraftObject;
    }

    public Class<?> getMinecraftClass() {
        return minecraftClass;
    }

    public GameSettings getGameSettings() {
        return gameSettings;
    }

    public static Minecraft getMinecraft() {
        return minecraft;
    }
}
