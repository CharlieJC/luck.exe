package me.charliejc.luck.wrappers;

import me.charliejc.luck.Luck;

import java.lang.reflect.InvocationTargetException;

public class ClientTickHandler {
    private static ClientTickHandler clientTickHandler;
    private Object clientTickHandlerObject;
    private Class<?> clientTickHandlerClass;

    public ClientTickHandler() {
        this.clientTickHandlerClass = Luck.getInstance().getDiscoverer().getClientTickHandler();

        try {
            this.clientTickHandlerObject = clientTickHandlerClass.getMethod("getInstance").invoke(null);

        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException ex) {
            ex.printStackTrace();
        }

        clientTickHandler = this;
    }

    public static ClientTickHandler getClientTickHandler() {
        return clientTickHandler;
    }

    public Object getClientTickHandlerObject() {
        return clientTickHandlerObject;
    }

    public Class<?> getClientTickHandlerClass() {
        return clientTickHandlerClass;
    }
}
