package me.charliejc.luck.wrappers;

import me.charliejc.luck.Luck;

import java.lang.reflect.Field;

public class PacketDispatcher {

    private Class<?> packetDispatcher;
    private static PacketDispatcher instance;
    private Field sendPacketToServerField;

    public PacketDispatcher() {
        try {
            packetDispatcher = Luck.getInstance().getDiscoverer().getPacketDispatcher();
            sendPacketToServerField = packetDispatcher.getField("sendPacketToServer");
        } catch (NoSuchFieldException ex){
            ex.printStackTrace();
        }
    }

    public void sendPacketToServer(Object packet) {

    }

    public static PacketDispatcher getInstance() {
        if (instance == null) {
            instance = new PacketDispatcher();
        }

        return instance;
    }
}
