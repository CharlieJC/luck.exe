package me.charliejc.luck.wrappers;

public class Mappings {


    //Methods
    public static final String getMinecraft = "func_71410_x";
    public static final String isEntityAlive = "func_70089_S";
    public static final String canEntityBeSeen = "func_70685_l";
    public static final String addChatMessage = "func_71035_c";
    public static final String isInvisible = "func_82150_aj";
    //Tessellator addVertex
    public static final String addVertex = "func_78377_a";


    //Classes
    public static final String CLASS_MINECRAFT = "net.minecraft.client.Minecraft";
    public static final String CLASS_ENTITY = "net.minecraft.entity.Entity";
    public static final String CLASS_WORLDCLIENT = "net.minecraft.client.multiplayer.WorldClient";
    public static final String CLASS_ENTITYLIVINGBASE = "net.minecraft.entity.EntityLivingBase";
    public static final String CLASS_MOVINGOBJECTPOSITION = "net.minecraft.util.MovingObjectPosition";
    public static final String CLASS_TESSELLATOR = "net.minecraft.client.renderer.Tessellator";
    public static final String CLASS_GAMESETTINGS = "net.minecraft.client.settings.GameSettings";
    public static final String CLASS_SESSION = "net.minecraft.util.Session";

    //forge
    public static final String CLASS_PACKETDISPATCHER = "cpw.mods.fml.common.network.PacketDispatcher";

    //Countercraft classes
    public static final String CLASS_CLIENT_TICK_HANDLER = "com.ferullogaming.countercraft.client.ClientTickHandler";


    //Fields
    public static final String objectMouseOver = "field_71476_x";
    public static final String inGameHasFocus = "field_71415_G";
    public static final String thePlayer = "field_71439_g";
    public static final String theWorld = "field_71441_e";
    public static final String rotationYaw = "field_70177_z";
    public static final String rotationPitch = "field_70125_A";
    public static final String posX = "field_70165_t";
    public static final String posY = "field_70163_u";
    public static final String posZ = "field_70161_v";
    public static final String playerEntities = "field_73010_i";
    public static final String entityHit = "field_72308_g";
    public static final String username = "field_71092_bJ";
    //Tessellator instance
    public static final String instance = "field_78398_a";
    //Minecraft gameSettings
    public static final String gameSettings = "field_71474_y";
    //GameSettings gammaSetting
    public static final String gammaSetting = "field_74333_Y";
}
